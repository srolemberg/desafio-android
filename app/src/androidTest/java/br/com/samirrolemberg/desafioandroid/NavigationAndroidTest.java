package br.com.samirrolemberg.desafioandroid;

import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.InstrumentationTestCase;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.net.HttpURLConnection;

import br.com.samirrolemberg.desafioandroid.activity.RepositoryActivity;
import br.com.samirrolemberg.desafioandroid.endpoint.RepositoryEndpoint;
import br.com.samirrolemberg.desafioandroid.helper.RetrofitHelper;
import br.com.samirrolemberg.desafioandroid.model.SearchRepositoriesResult;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class NavigationAndroidTest extends InstrumentationTestCase {

    @Rule
    public ActivityTestRule<RepositoryActivity> mActRule = new ActivityTestRule(RepositoryActivity.class);

    private MockWebServer server;

    private static String BASE_URL = "";

    @Before
    public void setUp() throws Exception {
        super.setUp();
        mActRule.getActivity().clearCache();

        server = new MockWebServer();
        server.start();
        injectInstrumentation(InstrumentationRegistry.getInstrumentation());
        BASE_URL = server.url("/").toString();
    }

    @Test
    public void testRequisicaoOK200Success() throws Exception {
        String fileName = "searchRepository.json";
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(RestServiceTestHelper.getStringFromFile(getInstrumentation().getContext(), fileName)));

        final Retrofit retrofit = RetrofitHelper.getInstance();
        RepositoryEndpoint endpoint = retrofit.create(RepositoryEndpoint.class);

        String query = "language:Java";
        String sort = "stars";
        Long page = 1l;

        Call<SearchRepositoriesResult> call = endpoint.searchRepositories(query, sort, page.toString());

        call.enqueue(new Callback<SearchRepositoriesResult>() {
            @Override
            public void onResponse(Call<SearchRepositoriesResult> call, Response<SearchRepositoriesResult> response) {
                if (response.body() != null && response.code() == HttpURLConnection.HTTP_OK) {
                    assertTrue(response.body() != null);
                }
            }

            @Override
            public void onFailure(Call<SearchRepositoriesResult> call, Throwable t) {

            }
        });
    }

}
