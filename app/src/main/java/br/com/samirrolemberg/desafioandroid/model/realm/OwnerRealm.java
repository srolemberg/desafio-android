package br.com.samirrolemberg.desafioandroid.model.realm;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class OwnerRealm implements RealmModel {

    private String login;
    @PrimaryKey
    private Long id;
    private String avatar_url;
    private String gravatar_id;
    private String url;
    private String received_events_url;
    private String type;

    public OwnerRealm() {
    }

    private OwnerRealm(Builder builder) {
        login = builder.login;
        id = builder.id;
        avatar_url = builder.avatar_url;
        gravatar_id = builder.gravatar_id;
        url = builder.url;
        received_events_url = builder.received_events_url;
        type = builder.type;
    }

    public static final class Builder {
        private String login;
        private Long id;
        private String avatar_url;
        private String gravatar_id;
        private String url;
        private String received_events_url;
        private String type;

        public Builder() {
        }

        public Builder withLogin(String val) {
            login = val;
            return this;
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withAvatar_url(String val) {
            avatar_url = val;
            return this;
        }

        public Builder withGravatar_id(String val) {
            gravatar_id = val;
            return this;
        }

        public Builder withUrl(String val) {
            url = val;
            return this;
        }

        public Builder withReceived_events_url(String val) {
            received_events_url = val;
            return this;
        }

        public Builder withType(String val) {
            type = val;
            return this;
        }

        public OwnerRealm build() {
            return new OwnerRealm(this);
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Owner{");
        sb.append("login='").append(login).append('\'');
        sb.append(", id=").append(id);
        sb.append(", avatar_url='").append(avatar_url).append('\'');
        sb.append(", gravatar_id='").append(gravatar_id).append('\'');
        sb.append(", url='").append(url).append('\'');
        sb.append(", received_events_url='").append(received_events_url).append('\'');
        sb.append(", type='").append(type).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public String getLogin() {
        return login;
    }

    public Long getId() {
        return id;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public String getGravatar_id() {
        return gravatar_id;
    }

    public String getUrl() {
        return url;
    }

    public String getReceived_events_url() {
        return received_events_url;
    }

    public String getType() {
        return type;
    }
}
