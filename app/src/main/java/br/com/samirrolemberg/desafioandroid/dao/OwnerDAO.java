package br.com.samirrolemberg.desafioandroid.dao;

import br.com.samirrolemberg.desafioandroid.model.realm.OwnerRealm;
import io.realm.Realm;
import io.realm.RealmResults;

public class OwnerDAO {

    private Realm realm;

    public OwnerDAO(Realm realm) {
        this.realm = realm;
    }

    public void removeAll() {
        RealmResults<OwnerRealm> results = realm.where(OwnerRealm.class)
                .findAll();
        results.deleteAllFromRealm();
    }
}
