package br.com.samirrolemberg.desafioandroid.adapter.holders;

import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import br.com.samirrolemberg.desafioandroid.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PullRequestViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.name)
    protected AppCompatTextView name;

    @BindView(R.id.description)
    protected AppCompatTextView description;

    @BindView(R.id.login)
    protected AppCompatTextView login;

    @BindView(R.id.avatar_url)
    protected AppCompatImageView avatar_url;

    @BindView(R.id.cardView)
    protected CardView cardView;

    @BindView(R.id.data)
    protected AppCompatTextView data;

    @BindView(R.id.full_name)
    protected AppCompatTextView full_name;

    public PullRequestViewHolder(View v) {
        super(v);
        ButterKnife.bind(this, v);
        getFull_name().setVisibility(View.GONE);
    }

    public AppCompatTextView getName() {
        return name;
    }

    public AppCompatTextView getDescription() {
        return description;
    }

    public AppCompatTextView getLogin() {
        return login;
    }

    public AppCompatImageView getAvatar_url() {
        return avatar_url;
    }

    public CardView getCardView() {
        return cardView;
    }

    public AppCompatTextView getData() {
        return data;
    }

    public AppCompatTextView getFull_name() {
        return full_name;
    }
}