package br.com.samirrolemberg.desafioandroid.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class PullRequest {

    private String html_url;
    private String id;
    @SerializedName("user")
    private Owner owner;
    private Date created_at;
    private Date updated_at;
    private String body;
    private String title;
    private String state;
    private Repository repository;

    private PullRequest() {
    }

    private PullRequest(Builder builder) {
        html_url = builder.html_url;
        id = builder.id;
        owner = builder.owner;
        created_at = builder.created_at;
        updated_at = builder.updated_at;
        body = builder.body;
        title = builder.title;
        state = builder.state;
        repository = builder.repository;
    }


    public static final class Builder {
        private String html_url;
        private String id;
        private Owner owner;
        private Date created_at;
        private Date updated_at;
        private String body;
        private String title;
        private String state;
        private Repository repository;

        public Builder() {
        }

        public Builder withHtml_url(String val) {
            html_url = val;
            return this;
        }

        public Builder withId(String val) {
            id = val;
            return this;
        }

        public Builder withOwner(Owner val) {
            owner = val;
            return this;
        }

        public Builder withCreated_at(Date val) {
            created_at = val;
            return this;
        }

        public Builder withUpdated_at(Date val) {
            updated_at = val;
            return this;
        }

        public Builder withBody(String val) {
            body = val;
            return this;
        }

        public Builder withTitle(String val) {
            title = val;
            return this;
        }

        public Builder withState(String val) {
            state = val;
            return this;
        }

        public Builder withRepository(Repository val) {
            repository = val;
            return this;
        }

        public PullRequest build() {
            return new PullRequest(this);
        }
    }

    public String getHtml_url() {
        return html_url;
    }

    public String getId() {
        return id;
    }

    public Owner getOwner() {
        return owner;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public String getBody() {
        return body;
    }

    public String getTitle() {
        return title;
    }

    public String getState() {
        return state;
    }

    public Repository getRepository() {
        return repository;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("PullRequest{");
        sb.append("html_url='").append(html_url).append('\'');
        sb.append(", id='").append(id).append('\'');
        sb.append(", owner=").append(owner);
        sb.append(", created_at=").append(created_at);
        sb.append(", updated_at=").append(updated_at);
        sb.append(", body='").append(body).append('\'');
        sb.append(", title='").append(title).append('\'');
        sb.append(", state='").append(state).append('\'');
        sb.append(", repository=").append(repository);
        sb.append('}');
        return sb.toString();
    }
}
