package br.com.samirrolemberg.desafioandroid.endpoint;

import java.util.List;

import br.com.samirrolemberg.desafioandroid.model.PullRequest;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface PullRequestsEndpoint {

    @GET("/repos/{owner}/{repo}/pulls")
    Call<List<PullRequest>> listPullRequests(@Path("owner") String owner, @Path("repo") String repo);

}
