package br.com.samirrolemberg.desafioandroid.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchRepositoriesResult {
    private Long total_count;
    private boolean incomplete_results;
    @SerializedName("items")
    private List<Repository> repositories;

    private SearchRepositoriesResult() {
    }

    private SearchRepositoriesResult(Builder builder) {
        total_count = builder.total_count;
        incomplete_results = builder.incomplete_results;
        repositories = builder.repositories;
    }


    public static final class Builder {
        private Long total_count;
        private boolean incomplete_results;
        private List<Repository> repositories;

        public Builder() {
        }

        public Builder withTotal_count(Long val) {
            total_count = val;
            return this;
        }

        public Builder withIncomplete_results(boolean val) {
            incomplete_results = val;
            return this;
        }

        public Builder withItems(List<Repository> val) {
            repositories = val;
            return this;
        }

        public SearchRepositoriesResult build() {
            return new SearchRepositoriesResult(this);
        }
    }

    public Long getTotal_count() {
        return total_count;
    }

    public boolean isIncomplete_results() {
        return incomplete_results;
    }

    public List<Repository> getRepositories() {
        return repositories;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Search{");
        sb.append("total_count=").append(total_count);
        sb.append(", incomplete_results=").append(incomplete_results);
        sb.append(", repositories=").append(repositories);
        sb.append('}');
        return sb.toString();
    }
}
