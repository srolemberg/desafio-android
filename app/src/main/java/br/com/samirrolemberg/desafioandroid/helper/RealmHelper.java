package br.com.samirrolemberg.desafioandroid.helper;

import br.com.samirrolemberg.desafioandroid.BuildConfig;
import br.com.samirrolemberg.desafioandroid.R;
import br.com.samirrolemberg.desafioandroid.util.CustomContext;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class RealmHelper {

    private RealmHelper() {
    }

    public static Realm getInstance() {
        final RealmConfiguration realmConfiguration;

        RealmConfiguration.Builder builder = new RealmConfiguration.Builder(CustomContext.getContext());
            builder.name(CustomContext.getContext().getResources().getString(R.string.nome_banco));
            if (BuildConfig.DEBUG) {
                builder.deleteRealmIfMigrationNeeded();
            }

        realmConfiguration = builder.build();

        return Realm.getInstance(realmConfiguration);
    }

}
