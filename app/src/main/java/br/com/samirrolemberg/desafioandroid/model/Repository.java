package br.com.samirrolemberg.desafioandroid.model;

import java.io.Serializable;

public class Repository implements Serializable{

    private Long id;
    private String name;
    private String description;
    private Long forks_count;
    private Long stargazers_count;
    private Owner owner;

    private Repository() {
    }

    private Repository(Builder builder) {
        id = builder.id;
        name = builder.name;
        description = builder.description;
        forks_count = builder.forks_count;
        stargazers_count = builder.stargazers_count;
        owner = builder.owner;
    }


    public static final class Builder {
        private Long id;
        private String name;
        private String description;
        private Long forks_count;
        private Long stargazers_count;
        private Owner owner;

        public Builder() {
        }

        public Builder withId(Long val) {
            id = val;
            return this;
        }

        public Builder withName(String val) {
            name = val;
            return this;
        }

        public Builder withDescription(String val) {
            description = val;
            return this;
        }

        public Builder withForks_count(Long val) {
            forks_count = val;
            return this;
        }

        public Builder withStargazers_count(Long val) {
            stargazers_count = val;
            return this;
        }

        public Builder withOwner(Owner val) {
            owner = val;
            return this;
        }

        public Repository build() {
            return new Repository(this);
        }
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Long getForks_count() {
        return forks_count;
    }

    public Long getStargazers_count() {
        return stargazers_count;
    }

    public Owner getOwner() {
        return owner;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Item{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", forks_count=").append(forks_count);
        sb.append(", stargazers_count=").append(stargazers_count);
        sb.append(", owner=").append(owner);
        sb.append('}');
        return sb.toString();
    }
}
