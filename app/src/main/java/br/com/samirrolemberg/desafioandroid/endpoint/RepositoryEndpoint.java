package br.com.samirrolemberg.desafioandroid.endpoint;

import br.com.samirrolemberg.desafioandroid.model.SearchRepositoriesResult;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RepositoryEndpoint {

    @GET("/search/repositories")
    Call<SearchRepositoriesResult> searchRepositories(@Query("q") String q, @Query("sort") String sort, @Query("page") String page);

}
