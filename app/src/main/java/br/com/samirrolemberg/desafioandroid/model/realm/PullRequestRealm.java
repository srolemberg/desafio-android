package br.com.samirrolemberg.desafioandroid.model.realm;

import java.util.Date;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class PullRequestRealm implements RealmModel {

    private String html_url;
    @PrimaryKey
    private String id;
    private OwnerRealm owner;
    private Date created_at;
    private Date updated_at;
    private String body;
    private String title;
    private String state;
    private RepositoryRealm repositoryRealm;

    public PullRequestRealm() {
    }

    private PullRequestRealm(Builder builder) {
        html_url = builder.html_url;
        id = builder.id;
        owner = builder.owner;
        created_at = builder.created_at;
        updated_at = builder.updated_at;
        body = builder.body;
        title = builder.title;
        state = builder.state;
        repositoryRealm = builder.repositoryRealm;
    }


    public static final class Builder {
        private String html_url;
        private String id;
        private OwnerRealm owner;
        private Date created_at;
        private Date updated_at;
        private String body;
        private String title;
        private String state;
        private RepositoryRealm repositoryRealm;

        public Builder() {
        }

        public Builder withHtml_url(String val) {
            html_url = val;
            return this;
        }

        public Builder withId(String val) {
            id = val;
            return this;
        }

        public Builder withOwner(OwnerRealm val) {
            owner = val;
            return this;
        }

        public Builder withCreated_at(Date val) {
            created_at = val;
            return this;
        }

        public Builder withUpdated_at(Date val) {
            updated_at = val;
            return this;
        }

        public Builder withBody(String val) {
            body = val;
            return this;
        }

        public Builder withTitle(String val) {
            title = val;
            return this;
        }

        public Builder withState(String val) {
            state = val;
            return this;
        }

        public Builder withRepositoryRealm(RepositoryRealm val) {
            repositoryRealm = val;
            return this;
        }

        public PullRequestRealm build() {
            return new PullRequestRealm(this);
        }
    }

    public String getHtml_url() {
        return html_url;
    }

    public String getId() {
        return id;
    }

    public OwnerRealm getOwner() {
        return owner;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public String getBody() {
        return body;
    }

    public String getTitle() {
        return title;
    }

    public String getState() {
        return state;
    }

    public RepositoryRealm getRepositoryRealm() {
        return repositoryRealm;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("PullRequestRealm{");
        sb.append("html_url='").append(html_url).append('\'');
        sb.append(", id='").append(id).append('\'');
        sb.append(", owner=").append(owner);
        sb.append(", created_at=").append(created_at);
        sb.append(", updated_at=").append(updated_at);
        sb.append(", body='").append(body).append('\'');
        sb.append(", title='").append(title).append('\'');
        sb.append(", state='").append(state).append('\'');
        sb.append(", repositoryRealm=").append(repositoryRealm);
        sb.append('}');
        return sb.toString();
    }
}
