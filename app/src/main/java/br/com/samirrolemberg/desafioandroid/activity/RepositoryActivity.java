package br.com.samirrolemberg.desafioandroid.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.samirrolemberg.desafioandroid.R;
import br.com.samirrolemberg.desafioandroid.adapter.RepositoryAdapter;
import br.com.samirrolemberg.desafioandroid.dao.OwnerDAO;
import br.com.samirrolemberg.desafioandroid.dao.PullRequestDAO;
import br.com.samirrolemberg.desafioandroid.dao.RepositoryDAO;
import br.com.samirrolemberg.desafioandroid.endpoint.RepositoryEndpoint;
import br.com.samirrolemberg.desafioandroid.helper.RealmHelper;
import br.com.samirrolemberg.desafioandroid.helper.RetrofitHelper;
import br.com.samirrolemberg.desafioandroid.model.Repository;
import br.com.samirrolemberg.desafioandroid.model.SearchRepositoriesResult;
import br.com.samirrolemberg.desafioandroid.model.error.GitError;
import br.com.samirrolemberg.desafioandroid.model.realm.OwnerRealm;
import br.com.samirrolemberg.desafioandroid.model.realm.RepositoryRealm;
import br.com.samirrolemberg.desafioandroid.util.Util;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RepositoryActivity extends AppCompatActivity {

    @BindView(R.id.swipeRefresh)
    protected SwipeRefreshLayout swipeRefresh;

    @BindView(R.id.listRepositories)
    protected RecyclerView listRepositories;

    @BindView(R.id.progressBar)
    protected ProgressBar progressBar;

    @BindView(R.id.fab)
    protected FloatingActionButton fab;

    private String query = "language:Java";
    private String sort = "stars";
    private Long page = 2l;

    private GridLayoutManager layoutManager;
    private RepositoryAdapter adapter;
    private List<Repository> itens = Collections.emptyList();
    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {

        @Override
        public void onRefresh() {
            clearCache();
            carregar(query, sort, "1", false);
        }
    };//usado na atualização

    private boolean loading = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_repositories);

        ButterKnife.bind(this);

        clearCache();

        itens = new ArrayList<>();
        itens.addAll(new RepositoryDAO(RealmHelper.getInstance()).findAll());


        layoutManager = new GridLayoutManager(this, 1, GridLayoutManager.VERTICAL, false);
        layoutManager.setSmoothScrollbarEnabled(true);
        adapter = new RepositoryAdapter(itens, this);
        listRepositories.setLayoutManager(layoutManager);
        listRepositories.setAdapter(adapter);

        swipeRefresh.setOnRefreshListener(refreshListener);
        swipeRefresh.setColorSchemeResources(R.color.accent);

        listRepositories.setOnScrollListener(getPaginationListener());

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                refreshListener.onRefresh();//carrega
            }
        }, 1 * 250);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exibirDialog(RepositoryActivity.this);
            }
        });

    }

    @NonNull
    private RecyclerView.OnScrollListener getPaginationListener() {
        return new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 5) {
                    if (loading) {
                        if ((layoutManager.getChildCount() + layoutManager.findFirstVisibleItemPosition()) >= layoutManager.getItemCount()) {
                            loading = false;
                            if (progressBar.getVisibility() != View.VISIBLE) {
                                carregar(query, sort, page.toString(), true);
                            }
                        }
                    }
                }
            }
        };
    }

    public void clearCache() {
        Realm realm = RealmHelper.getInstance();

        realm.beginTransaction();

        new OwnerDAO(realm).removeAll();
        new RepositoryDAO(realm).removeAll();
        new PullRequestDAO(realm).removeAll();

        realm.commitTransaction();
    }

    public Long getPage() {
        return page;
    }

    public void setPage(Long page) {
        this.page = page;
    }

    public SwipeRefreshLayout getSwipeRefresh() {
        return swipeRefresh;
    }

    private void carregar(String query, String sort, String page, final boolean pagination) {
        if (!pagination) {
            swipeRefresh.setRefreshing(true);
        } else {
            loading = true;
            progressBar.setVisibility(View.VISIBLE);
        }

        final Retrofit retrofit = RetrofitHelper.getInstance();
        RepositoryEndpoint endpoint = retrofit.create(RepositoryEndpoint.class);

        Call<SearchRepositoriesResult> call = endpoint.searchRepositories(query, sort, page);

        call.enqueue(new Callback<SearchRepositoriesResult>() {
            @Override
            public void onResponse(Call<SearchRepositoriesResult> call, Response<SearchRepositoriesResult> response) {
                if (!pagination) {
                    swipeRefresh.setRefreshing(false);
                } else {
                    loading = true;
                    progressBar.setVisibility(View.GONE);
                }

                if (response.body() != null && response.code() == HttpURLConnection.HTTP_OK) {
                    if (!pagination) {//se é pulltorefresh
                        setPage(2l);//reseta
                    } else {//se é paginação, aumenta
                        setPage(getPage() + 1);
                    }
                    SearchRepositoriesResult busca = response.body();

                    Realm realm = RealmHelper.getInstance();

                    realm.beginTransaction();

                    RealmList<RepositoryRealm> lista = new RealmList<>();

                    for (Repository item : busca.getRepositories()) {
                        OwnerRealm ownerRealm = new OwnerRealm.Builder()
                                .withId(item.getOwner().getId())
                                .withAvatar_url(item.getOwner().getAvatar_url())
                                .withGravatar_id(item.getOwner().getGravatar_id())
                                .withLogin(item.getOwner().getLogin())
                                .withReceived_events_url(item.getOwner().getReceived_events_url())
                                .withType(item.getOwner().getType())
                                .withUrl(item.getOwner().getUrl())
                                .build();
                        lista.add(
                                new RepositoryRealm.Builder()
                                        .withDescription(item.getDescription())
                                        .withForks_count(item.getForks_count())
                                        .withId(item.getId())
                                        .withName(item.getName())
                                        .withOwner(ownerRealm)
                                        .withStargazers_count(item.getStargazers_count())
                                        .build()
                        );
                    }

                    realm.copyToRealmOrUpdate(lista);

                    realm.commitTransaction();

                    itens.removeAll(itens);
                    itens.addAll(new RepositoryDAO(realm).findAll());
                    adapter.notifyDataSetChanged();
                } else {
                    try {
                        GitError gitError = (GitError) retrofit.responseBodyConverter(GitError.class, GitError.class.getAnnotations()).convert(response.errorBody());
                        Util.showErrorSnack(RepositoryActivity.this).setText(gitError.getMessage()).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                        Util.showErrorSnack(RepositoryActivity.this).setText(e.getMessage()).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<SearchRepositoriesResult> call, Throwable t) {
                if (!pagination) {
                    swipeRefresh.setRefreshing(false);
                } else {
                    loading = true;
                    progressBar.setVisibility(View.GONE);
                }
                Util.showErrorSnack(RepositoryActivity.this).setText(t.getMessage()).show();

            }
        });
    }

    public void exibirDialog(final Activity context) {
        final View view = context.getLayoutInflater().inflate(R.layout.dlg_sobre, null);

        final AppCompatImageView image = ButterKnife.findById(view, R.id.image);
        final AppCompatButton bitButton = ButterKnife.findById(view, R.id.bitButton);
        final AppCompatButton linkButton = ButterKnife.findById(view, R.id.linkButton);

        final AlertDialog dialog = new AlertDialog.Builder(context)
                .setCancelable(true)
                .setView(view)
                .create();

        final String avatar = getString(R.string.avatar);
        final String linkedIn = getString(R.string.linkedin_url);
        final String bitbucket = getString(R.string.bitbucket_url);

        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Glide.with(RepositoryActivity.this).load(avatar).into(image);
            }
        });

        linkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse(linkedIn);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                RepositoryActivity.this.startActivity(intent);
            }
        });
        bitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse(bitbucket);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                RepositoryActivity.this.startActivity(intent);
            }
        });

        dialog.show();

    }
}
