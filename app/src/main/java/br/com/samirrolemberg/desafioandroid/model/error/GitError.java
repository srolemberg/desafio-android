package br.com.samirrolemberg.desafioandroid.model.error;

public class GitError {

    private String message;
    private String documentation_url;

    private GitError() {
    }

    private GitError(Builder builder) {
        message = builder.message;
        documentation_url = builder.documentation_url;
    }

    public static final class Builder {
        private String message;
        private String documentation_url;

        public Builder() {
        }

        public Builder withMessage(String val) {
            message = val;
            return this;
        }

        public Builder withDocumentation_url(String val) {
            documentation_url = val;
            return this;
        }

        public GitError build() {
            return new GitError(this);
        }
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("GitError{");
        sb.append("message='").append(message).append('\'');
        sb.append(", documentation_url='").append(documentation_url).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public String getMessage() {
        return message;
    }

    public String getDocumentation_url() {
        return documentation_url;
    }
}
