package br.com.samirrolemberg.desafioandroid.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.samirrolemberg.desafioandroid.model.Repository;
import br.com.samirrolemberg.desafioandroid.model.Owner;
import br.com.samirrolemberg.desafioandroid.model.realm.RepositoryRealm;
import io.realm.Realm;
import io.realm.RealmResults;

public class RepositoryDAO {

    private Realm realm;

    public RepositoryDAO(Realm realm) {
        this.realm = realm;
    }

    public List<Repository> findAll() {
        RealmResults<RepositoryRealm> results = realm.where(RepositoryRealm.class)
                .findAll();
        List<Repository> list = new ArrayList<>();
        if (results.size() > 0) {
            for (int i = 0; i < results.size(); i++) {
                RepositoryRealm objRealm = results.get(i);


                Repository obj = new Repository.Builder()
                        .withForks_count(objRealm.getForks_count())
                        .withId(objRealm.getId())
                        .withDescription(objRealm.getDescription())
                        .withName(objRealm.getName())
                        .withStargazers_count(objRealm.getStargazers_count())
                        .withOwner(
                                new Owner.Builder()
                                        .withId(objRealm.getOwner().getId())
                                        .withGravatar_id(objRealm.getOwner().getGravatar_id())
                                        .withAvatar_url(objRealm.getOwner().getAvatar_url())
                                        .withUrl(objRealm.getOwner().getUrl())
                                        .withLogin(objRealm.getOwner().getLogin())
                                        .withReceived_events_url(objRealm.getOwner().getReceived_events_url())
                                        .withType(objRealm.getOwner().getType())
                                        .build()
                        )
                        .build();

                list.add(obj);
            }
            return list;
        } else {
            return list;
        }
    }

    public void removeAll() {
        RealmResults<RepositoryRealm> results = realm.where(RepositoryRealm.class)
                .findAll();
        results.deleteAllFromRealm();
    }

}
