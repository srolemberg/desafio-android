package br.com.samirrolemberg.desafioandroid.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.samirrolemberg.desafioandroid.R;
import br.com.samirrolemberg.desafioandroid.adapter.PullRequestAdapter;
import br.com.samirrolemberg.desafioandroid.dao.PullRequestDAO;
import br.com.samirrolemberg.desafioandroid.endpoint.PullRequestsEndpoint;
import br.com.samirrolemberg.desafioandroid.enums.PullRequestState;
import br.com.samirrolemberg.desafioandroid.helper.RealmHelper;
import br.com.samirrolemberg.desafioandroid.helper.RetrofitHelper;
import br.com.samirrolemberg.desafioandroid.model.PullRequest;
import br.com.samirrolemberg.desafioandroid.model.Repository;
import br.com.samirrolemberg.desafioandroid.model.error.GitError;
import br.com.samirrolemberg.desafioandroid.model.realm.OwnerRealm;
import br.com.samirrolemberg.desafioandroid.model.realm.PullRequestRealm;
import br.com.samirrolemberg.desafioandroid.model.realm.RepositoryRealm;
import br.com.samirrolemberg.desafioandroid.util.Util;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class PullRequestActivity extends AppCompatActivity {

    @BindView(R.id.swipeRefresh)
    protected SwipeRefreshLayout swipeRefresh;

    @BindView(R.id.listPullRequests)
    protected RecyclerView listPullRequests;

    private GridLayoutManager layoutManager;
    private PullRequestAdapter adapter;
    private List<PullRequest> itens = Collections.emptyList();

    private Repository item = null;

    @BindView(R.id.open)
    protected AppCompatTextView open;

    @BindView(R.id.close)
    protected AppCompatTextView close;

    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {

        @Override
        public void onRefresh() {
            carregar(item.getOwner().getLogin(), item.getName());
        }
    };//usado na atualização

    private void carregar(String owner, String repo) {
        swipeRefresh.setRefreshing(true);

        final Retrofit retrofit = RetrofitHelper.getInstance();
        PullRequestsEndpoint endpoint = retrofit.create(PullRequestsEndpoint.class);

        Call<List<PullRequest>> call = endpoint.listPullRequests(owner, repo);

        call.enqueue(new Callback<List<PullRequest>>() {
            @Override
            public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {
                swipeRefresh.setRefreshing(false);
                if (response.body() != null && response.code() == HttpURLConnection.HTTP_OK) {
                    List<PullRequest> pullRequests = response.body();

                    if (pullRequests.size() > 0) {
                        Realm realm = RealmHelper.getInstance();

                        realm.beginTransaction();

                        RealmList<PullRequestRealm> lista = new RealmList<>();

                        OwnerRealm itemOwnerRealm = new OwnerRealm.Builder()
                                .withId(item.getOwner().getId())
                                .withAvatar_url(item.getOwner().getAvatar_url())
                                .withGravatar_id(item.getOwner().getGravatar_id())
                                .withLogin(item.getOwner().getLogin())
                                .withReceived_events_url(item.getOwner().getReceived_events_url())
                                .withType(item.getOwner().getType())
                                .withUrl(item.getOwner().getUrl())
                                .build();
                        RepositoryRealm repositoryRealm = new RepositoryRealm.Builder()
                                .withDescription(item.getDescription())
                                .withOwner(itemOwnerRealm)
                                .withStargazers_count(item.getId())
                                .withForks_count(item.getForks_count())
                                .withName(item.getName())
                                .withId(item.getId())
                                .build();
                        for (PullRequest pullRequest : pullRequests) {
                            OwnerRealm ownerRealm = new OwnerRealm.Builder()
                                    .withId(pullRequest.getOwner().getId())
                                    .withAvatar_url(pullRequest.getOwner().getAvatar_url())
                                    .withGravatar_id(pullRequest.getOwner().getGravatar_id())
                                    .withLogin(pullRequest.getOwner().getLogin())
                                    .withReceived_events_url(pullRequest.getOwner().getReceived_events_url())
                                    .withType(pullRequest.getOwner().getType())
                                    .withUrl(pullRequest.getOwner().getUrl())
                                    .build();
                            lista.add(new PullRequestRealm.Builder()
                                    .withBody(pullRequest.getBody())
                                    .withCreated_at(pullRequest.getCreated_at())
                                    .withHtml_url(pullRequest.getHtml_url())
                                    .withId(pullRequest.getId())
                                    .withTitle(pullRequest.getTitle())
                                    .withUpdated_at(pullRequest.getUpdated_at())
                                    .withOwner(ownerRealm)
                                    .withRepositoryRealm(repositoryRealm)
                                    .withState(pullRequest.getState())
                                    .build());
                        }

                        realm.copyToRealmOrUpdate(lista);

                        realm.commitTransaction();

                        itens.removeAll(itens);
                        itens.addAll(new PullRequestDAO(realm).findAllByRepo(item.getId()));
                        adapter.notifyDataSetChanged();
                        countPullRequest();
                    } else {
                        Util.showErrorSnack(PullRequestActivity.this).setText(R.string.no_results).show();
                    }
                } else {
                    try {
                        GitError gitError = (GitError) retrofit.responseBodyConverter(GitError.class, GitError.class.getAnnotations()).convert(response.errorBody());
                        Util.showErrorSnack(PullRequestActivity.this).setText(gitError.getMessage()).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                        Util.showErrorSnack(PullRequestActivity.this).setText(e.getMessage()).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<PullRequest>> call, Throwable t) {
                swipeRefresh.setRefreshing(false);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_pull_request);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);//habilita o botão back na tela

        if (getIntent() != null) {
            item = (Repository) getIntent().getSerializableExtra(getString(R.string.intent_item));
        }
        if (item != null) {
            getSupportActionBar().setTitle(item.getName());
        }

        countPullRequest();

        itens = new ArrayList<>();
        itens.addAll(new PullRequestDAO(RealmHelper.getInstance()).findAllByRepo(item.getId()));

        layoutManager = new GridLayoutManager(this, 1, GridLayoutManager.VERTICAL, false);
        layoutManager.setSmoothScrollbarEnabled(true);
        adapter = new PullRequestAdapter(itens, this);
        listPullRequests.setLayoutManager(layoutManager);
        listPullRequests.setAdapter(adapter);

        swipeRefresh.setOnRefreshListener(refreshListener);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                refreshListener.onRefresh();//carrega
            }
        }, 1 * 250);
    }

    private void countPullRequest() {
        PullRequestDAO pullRequestDAO = new PullRequestDAO(RealmHelper.getInstance());
        open.setText(Long.toString(pullRequestDAO.countPullRequestByState(item.getId(), PullRequestState.OPEN)));
        close.setText(Long.toString(pullRequestDAO.countPullRequestByState(item.getId(), PullRequestState.CLOSED)));
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {//botão boltar do topo
                this.onBackPressed();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
