package br.com.samirrolemberg.desafioandroid.util;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

import java.util.Date;

import br.com.samirrolemberg.desafioandroid.R;

public class Util {

    public static CharSequence time_24_mask(Date date) {
        java.text.DateFormat d = android.text.format.DateFormat.getTimeFormat(CustomContext.getContext());
        return d.format(date);
    }

    public static CharSequence date_mask(Date date) {
        java.text.DateFormat d = android.text.format.DateFormat.getDateFormat(CustomContext.getContext());
        return d.format(date);
    }

    public static Snackbar showErrorSnack(Activity activity) {
        String texto = "";
        Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), texto, Snackbar.LENGTH_LONG);
        View view = snackbar.getView();
        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(CustomContext.getContext().getResources().getColor(R.color.yellow));
        tv.setTypeface(Typeface.DEFAULT_BOLD);
        return snackbar;
    }

}
