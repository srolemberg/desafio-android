package br.com.samirrolemberg.desafioandroid.adapter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.List;

import br.com.samirrolemberg.desafioandroid.R;
import br.com.samirrolemberg.desafioandroid.adapter.holders.PullRequestViewHolder;
import br.com.samirrolemberg.desafioandroid.model.PullRequest;
import br.com.samirrolemberg.desafioandroid.util.Util;

public class PullRequestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<PullRequest> itens;
    private Activity activity;

    public PullRequestAdapter(List<PullRequest> itens, Activity activity) {
        super();
        this.itens = itens;
        this.activity = activity;
    }

    @Override
    public int getItemCount() {
        return itens.size();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder paramVH, final int position) {
        final PullRequestViewHolder holder = (PullRequestViewHolder) paramVH;
        if (!itens.isEmpty() && itens.get(0).getId() != null) {
            holder.getDescription().setText(itens.get(position).getBody());
            holder.getName().setText(itens.get(position).getTitle());
            holder.getLogin().setText(itens.get(position).getOwner() == null ? "" : itens.get(position).getOwner().getLogin());

            Glide.with(activity).load(itens.get(position).getOwner() == null ? "" : itens.get(position).getOwner().getAvatar_url()).into(holder.getAvatar_url());

            String data = null;
            String hora = null;

            data = Util.date_mask(itens.get(position).getCreated_at()).toString();
            hora = Util.time_24_mask(itens.get(position).getCreated_at()).toString();

            holder.getData().setText(activity.getString(R.string.lbl_hour_date_create, data, hora));

        }
        //listners
        holder.getCardView().setOnClickListener(getCardViewOnClick(paramVH, position));
    }

    @NonNull
    private View.OnClickListener getCardViewOnClick(RecyclerView.ViewHolder paramVH, final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse(itens.get(position).getHtml_url());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                activity.startActivity(intent);
            }
        };
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PullRequestViewHolder(LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.adp_pull_requests, parent, false));
    }

}
