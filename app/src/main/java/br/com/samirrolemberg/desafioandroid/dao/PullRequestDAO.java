package br.com.samirrolemberg.desafioandroid.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.samirrolemberg.desafioandroid.enums.PullRequestState;
import br.com.samirrolemberg.desafioandroid.model.Owner;
import br.com.samirrolemberg.desafioandroid.model.PullRequest;
import br.com.samirrolemberg.desafioandroid.model.realm.PullRequestRealm;
import br.com.samirrolemberg.desafioandroid.model.realm.RepositoryRealm;
import io.realm.Realm;
import io.realm.RealmResults;

public class PullRequestDAO {

    private Realm realm;

    public PullRequestDAO(Realm realm) {
        this.realm = realm;
    }

    public List<PullRequest> findAllByRepo(Long id) {
        RealmResults<PullRequestRealm> results = realm.where(PullRequestRealm.class)
                .equalTo("repositoryRealm.id", id)
                .findAll();
        List<PullRequest> list = new ArrayList<>();
        if (results.size() > 0) {
            for (int i = 0; i < results.size(); i++) {
                PullRequestRealm objRealm = results.get(i);


                PullRequest obj = new PullRequest.Builder()
                        .withUpdated_at(objRealm.getUpdated_at())
                        .withTitle(objRealm.getTitle())
                        .withId(objRealm.getId())
                        .withCreated_at(objRealm.getCreated_at())
                        .withBody(objRealm.getBody())
                        .withHtml_url(objRealm.getHtml_url())
                        .withOwner(
                                new Owner.Builder()
                                        .withId(objRealm.getOwner().getId())
                                        .withGravatar_id(objRealm.getOwner().getGravatar_id())
                                        .withAvatar_url(objRealm.getOwner().getAvatar_url())
                                        .withUrl(objRealm.getOwner().getUrl())
                                        .withLogin(objRealm.getOwner().getLogin())
                                        .withReceived_events_url(objRealm.getOwner().getReceived_events_url())
                                        .withType(objRealm.getOwner().getType())
                                        .build()
                        )
                        .build();

                list.add(obj);
            }
            return list;
        } else {
            return list;
        }
    }

    public void removeAll() {
        RealmResults<PullRequestRealm> results = realm.where(PullRequestRealm.class)
                .findAll();
        results.deleteAllFromRealm();
    }

    //open/closed
    public Long countPullRequestByState(Long repositoryRealm_id, PullRequestState state){
        return realm.where(PullRequestRealm.class)
                .equalTo("repositoryRealm.id", repositoryRealm_id)
                .equalTo("state", state.toString())
                .count();
    }

}
