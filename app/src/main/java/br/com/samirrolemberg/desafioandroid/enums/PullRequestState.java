package br.com.samirrolemberg.desafioandroid.enums;

public enum PullRequestState {

	OPEN("open"),
	CLOSED("closed");

	private final String state;

	PullRequestState(String s) {
		state = s;
	}

	public boolean equalsName(String otherName) {
		return (otherName == null) ? false : state.equals(otherName);
	}

	public String toString() {
		return this.state;
	}
}
