package br.com.samirrolemberg.desafioandroid.helper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import br.com.samirrolemberg.desafioandroid.BuildConfig;
import br.com.samirrolemberg.desafioandroid.R;
import br.com.samirrolemberg.desafioandroid.util.CustomContext;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitHelper {

    public static int TIME_RELEASE = 90;
    public static int TIME_DEBUG = 30;

    private RetrofitHelper() {
    }

    public static Retrofit getInstance() {
        return getInstance(null);
    }

    public static Retrofit getInstance(Interceptor interceptor) {
        final int time = BuildConfig.DEBUG ? TIME_DEBUG : TIME_RELEASE;
        final OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(time, TimeUnit.SECONDS);
        builder.connectTimeout(time, TimeUnit.SECONDS);
        builder.addInterceptor(getDefaultHeader());
        if (interceptor != null) {//header adicional externo
            builder.addInterceptor(interceptor);
        }
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(loggingInterceptor);
        }
        Gson gson = new GsonBuilder()
                .setDateFormat(CustomContext.getContext().getString(R.string.xsd_datetime_format))
                .create();

        final OkHttpClient okHttpClient = builder.build();
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(CustomContext.getContext().getString(R.string.url))
                .addConverterFactory(GsonConverterFactory.create(gson).create())
                .client(okHttpClient)
                .build();
        return retrofit;
    }

    private static Interceptor getDefaultHeader() {
        return new Interceptor() {

            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request.Builder requestBuilder;
                Request request;

                requestBuilder = chain.request().newBuilder();

                requestBuilder.header(CustomContext.getContext().getString(R.string.header_contenttype), CustomContext.getContext().getString(R.string.header_contenttype_value));
                requestBuilder.header(CustomContext.getContext().getString(R.string.header_accept), CustomContext.getContext().getString(R.string.header_contentgithub_value));

                request = requestBuilder.build();
                return chain.proceed(request);
            }
        };
    }
}
