package br.com.samirrolemberg.desafioandroid.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.List;

import br.com.samirrolemberg.desafioandroid.R;
import br.com.samirrolemberg.desafioandroid.activity.PullRequestActivity;
import br.com.samirrolemberg.desafioandroid.adapter.holders.RepositoryViewHolder;
import br.com.samirrolemberg.desafioandroid.model.Repository;

public class RepositoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Repository> itens;
    private Activity activity;

    public RepositoryAdapter(List<Repository> itens, Activity activity) {
        super();
        this.itens = itens;
        this.activity = activity;
    }

    @Override
    public int getItemCount() {
        return itens.size();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder paramVH, final int position) {
        final RepositoryViewHolder holder = (RepositoryViewHolder) paramVH;
        if (!itens.isEmpty() && itens.get(0).getId() != null) {
            holder.getDescription().setText(itens.get(position).getDescription());
            holder.getName().setText(itens.get(position).getName());
            holder.getLogin().setText(itens.get(position).getOwner() == null ? "" : itens.get(position).getOwner().getLogin());
            holder.getForks_count().setText(Long.toString(itens.get(position).getForks_count()));
            holder.getStargazers_count().setText(Long.toString(itens.get(position).getStargazers_count()));

            Glide.with(activity).load(itens.get(position).getOwner() == null ? "" : itens.get(position).getOwner().getAvatar_url()).into(holder.getAvatar_url());
        }
        //listners
        holder.getCardView().setOnClickListener(getCardViewOnClick(paramVH, position));
    }

    @NonNull
    private View.OnClickListener getCardViewOnClick(RecyclerView.ViewHolder paramVH, final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, PullRequestActivity.class);
                intent.putExtra(activity.getString(R.string.intent_item), itens.get(position));
                activity.startActivity(intent);
            }
        };
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RepositoryViewHolder(LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.adp_repositories, parent, false));
    }

}
